package duplicates

import (
	"errors"
	"fmt"
	"io/fs"
	"math"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

func Exists(filename string) (bool, error) {
	_, err := os.Stat(filename)
	if err == nil {
		return true, nil
	}
	if errors.Is(err, fs.ErrNotExist) {
		return false, nil
	}
	var mu bool
	return mu, err
}

// MkFilename creates a filename for a given duplicate N.
func MkFilename(basename string, dupe int, ext string) string {
	return fmt.Sprintf("%s(%d)%s", basename, dupe, ext)
}

var duplicateRegex = regexp.MustCompile(`\((\d+)\)`)

// Duplicate extracts the duplicate number in trailer position from a filename.
// and returns the filename without duplicate, and the numeric duplicate.
func Duplicate(filename string) (string, int) {
	match := duplicateRegex.FindStringSubmatch(filename)
	if len(match) == 0 {
		return filename, 0
	}
	lastGroup := match[len(match)-1]
	n, err := strconv.Atoi(lastGroup)
	if err != nil {
		return filename, 0
	}
	return strings.TrimSuffix(filename, match[0]), n
}

// Next creates the following duplicate.
func Next(filename string) string {
	ext := filepath.Ext(filename) // ext includes dot, if any
	raw := strings.TrimSuffix(filename, ext)
	s, n := Duplicate(raw)
	if s == raw {
		// no duplicate yet, create first one
		n = 1
	}
	return MkFilename(s, n+1, ext)
}

// NextAvailable returns a filename indicating a numbered duplicate, that is
// 'filename(2).ext' for 'filename.ext'. If 'filename(2).ext' exists,
// 'filename(n).ext' is used until an n that has no existing file.
// This function is by its interaction with the filesystem race prone, different
// processes may have created files in the meantime.
func NextAvailable(filename string) (string, error) {
	exists, err := Exists(filename)
	if err != nil {
		return filename, err
	}
	if !exists {
		return filename, err
	}

	ext := filepath.Ext(filename)
	raw := strings.TrimSuffix(filename, ext)
	for i := 2; i < math.MaxInt; i++ {
		new := MkFilename(raw, i, ext)
		exists, err := Exists(new)
		if err != nil {
			return filename, err
		}
		if !exists {
			return new, nil
		}
	}
	return filename, fmt.Errorf("maximum number of duplicates reached: %d",
		math.MaxInt)
}
