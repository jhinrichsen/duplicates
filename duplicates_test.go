package duplicates

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"testing"
)

func ExampleDuplicate() {
	s, n := Duplicate("test(2)")
	fmt.Printf("%s %d", s, n)
	// Output: "test 2"
}

func ExampleMkFilename() {
	fmt.Println(MkFilename("test", 3, ".txt"))
	// Output: "test (3).txt"
}

type DupeTest struct {
	from         string
	hasDuplicate bool
	duplicate    int
	into         string
}

var tests = []DupeTest{
	{"test", false, 0, "test(2)"},
	{"test (2)", true, 2, "test (3)"},
	{"test.txt", false, 0, "test(2).txt"},
	{"test(2).txt", true, 2, "test(3).txt"},
	{"test(3).txt", true, 3, "test(4).txt"},
}

func multirun(t *testing.T, f func(u *testing.T, test DupeTest)) {
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			f(t, test)
		})
	}
}

func TestNext(t *testing.T) {
	multirun(t, func(t *testing.T, test DupeTest) {
		want := test.into
		got := Next(test.from)
		if want != got {
			t.Fatalf("want %q but got %q", want, got)
		}
	})
}

func TestDuplicate(t *testing.T) {
	multirun(t, func(t *testing.T, test DupeTest) {
		if test.hasDuplicate {
			want := test.duplicate
			_, got := Duplicate(test.from)
			if want != got {
				t.Fatal("no duplicate must result in error")
			}
			if want != got {
				t.Fatalf("want %d but got %d", want, got)
			}
		} else {
			want := test.from
			got, _ := Duplicate(test.from)
			if want != got {
				t.Fatalf("want %q but got %q", want, got)
			}
		}
	})
}

func TestNextAvailable(t *testing.T) {
	const want = "test(4).txt"

	// setup
	dir, err := os.MkdirTemp("", "duplicates-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir) // tear down

	const s = "test.txt"
	for _, filename := range []string{s, "test(2).txt", "test(3).txt"} {
		file := filepath.Join(dir, filename)
		if err := os.WriteFile(file, []byte(""), 0666); err != nil {
			t.Fatal(err)
		}
	}
	got, err := NextAvailable(filepath.Join(dir, s))
	if err != nil {
		t.Fatal(err)
	}
	got = filepath.Base(got)
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

func TestMkFilename(t *testing.T) {
	const want = "test(3).txt"
	got := MkFilename("test", 3, ".txt")
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}
